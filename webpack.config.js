var path = require('path');
var webpack = require('webpack');

var config = {
	entry: {
		main: './main/app/index.js',
		vendor: [
			"history",
			'underscore',
			'mobx',
			'mobx-react',
			"react-bootstrap-slider",
			"react-bootstrap-toggle",
			"react-checkbox-group",
			"react-datepicker",
			"react-dnd",
			"react-dnd-html5-backend",
			"react-router",
			"react-router-bootstrap",
		]
	},
	output: {
		path: path.join(__dirname, '/web'),
		filename: 'app/[name].bundle.js',
		chunkFilename: 'app/[id].bundle.js',
		publicPath: '/'
	},
	module: {
		exprContextCritical: false,
		rules: [
			{
				exclude: /(node_modules|bower_components)/,
				test: /\.jsx?/,
				loader: 'babel-loader'
			},
			{
				test: /\.less$/,
				loader: "style-loader!css-loader!less-loader"
			},
			{
				test: /\.css$/,
				loader: "style-loader!css-loader!postcss-loader"
			}
		],
	},
	resolve: {
		extensions: [".js", ".jsx"]
	},
	externals: {
		"react": "React",
		"react-dom": "ReactDOM",
		"react-bootstrap": "ReactBootstrap",
		"moment": "moment",
		"xlsx": "xlsx"
	},
	plugins: [
		//new webpack.optimize.CommonsChunkPlugin({ name: 'vendor' }) -- deprecated
	],
	optimization: {
		splitChunks: {
			cacheGroups: {
				default: false,
				commons: {
					test: /[\\/]node_modules[\\/]/,
					name: "vendor",
					chunks: "all"
				}
			}
		}
	}
};

if (process.env.NODE_ENV === 'production') {
}
else {
	//config.plugins.push(new webpack.HotModuleReplacementPlugin());
	config.devtool = "source-map";
	config.devServer = {
		contentBase: './web',
		port: 8008,
		historyApiFallback: {
			index: 'index.html'
		},
		headers: { "Access-Control-Allow-Origin": "*" }
	};
}

module.exports = config;
