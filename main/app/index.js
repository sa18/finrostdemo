import 'babel-polyfill';
import ReactDOM from 'react-dom';
import * as mobx from 'mobx';
import flowRuntime from 'flow-runtime';
import flowRuntimeMobx from 'flow-runtime-mobx';
import { BrowserRouter } from 'react-router-dom';
flowRuntimeMobx(flowRuntime, mobx);
//mobx.useStrict(true); -- too annoying
import App0 from './App0';
import App1 from './App1';
import App2 from './App2';
import App3 from './App3';
import App4 from './App4';
ReactDOM.render(<App0 />, document.getElementById('root'));
