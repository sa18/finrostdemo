//@flow
import { observable, computed } from 'mobx';
import { CurrencyInfo } from './../../api/types';
import { fetchCurrenciesList } from './../../api/fetch';
import { reaction } from 'mobx';

export class App3Store {

	@observable currList: ?CurrencyInfo[];
	@observable workingDate = moment();
	@observable selectedCurrCode: ?string = null;

	constructor() {
		let self = this;
		fetchCurrenciesList().then((data) => {
			self.currList = data
		});

		/*
		reaction(
			() => [self.workingDate],
			() => {
				console.debug("Изменена рабочая дата: " + moment(this.workingDate).format("YYYY-MM-DD"))
			}
		);

		reaction(
			() => [self.selectedCurrCode],
			() => {
				console.debug("Изменена выбранная валюта: " + this.selectedCurrCode)
			}
		);*/

	}

	dispose() {
	}

	@computed get isDataReady(): boolean {
		return this.currList != null;
	}

	renderSimpleList() {
		return <div>
			<ul>
				{
					this.currList.map(it => <li key={it.code}>{it.code} <br /> {it.description}</li>)
				}
			</ul>
		</div>
	}
}
