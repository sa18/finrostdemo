import React, { Children } from 'react';
import * as R from 'react-bootstrap';
import { observer } from 'mobx-react';
import { observable, reaction, runInAction } from 'mobx';
import { App3Store } from './App3Store';

export interface CurrencyChooserProps {
	store: App3Store;
}

@observer
export class CurrencyChooser extends React.Component<CurrencyChooserProps> {

	onChange(v) {
		this.props.store.selectedCurrCode = v.target.value;
	}

	render() {
		return <R.FormControl componentClass="select"
			value={this.props.store.selectedCurrCode || ""}
			onChange={this.onChange.bind(this)}>

			{this.props.store.currList.map(it => <option key={it.code} value={it.code}>
				{it.code}
			</option>)}

		</R.FormControl>
	}
}
