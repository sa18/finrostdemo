import * as R from 'react-bootstrap';
import React, { Children } from 'react';
import { observer, inject } from 'mobx-react';
import { observable, reaction, runInAction } from 'mobx';
import DatePicker from 'react-datepicker';
import { Util } from './../../util/index';
import { App4Store } from './App4Store';

export interface WorkingDateProps {
	enableRange: boolean;
	disabled: boolean;
	store: App4Store;
}

@inject("store")
@observer
export class WorkingDateChooser extends React.Component<WorkingDateProps> {
	@observable _selectedDate: moment = new moment();
	@observable _selectedDateStart: moment = new moment();
	static defaultProps = { enableRange: false, disabled: false };
	constructor() {
		super();
		let self = this;
		reaction(
			() => [self.props && self.props.store.workingDate],
			() => runInAction(() => {
				if (self.props) {
					self._selectedDate = self.props.store.workingDate;
					self._selectedDateStart = self.props.store.workingDate;
				}
			}),
			true
		)
	}
	render() {

		let self = this;
		function evtDateUpdated(d) {
			if (d) {
				runInAction(() => {
					self._selectedDate = d;
					self.props.store.workingDate = d;
				});
				// close dropdown
				Util.simulateClick();
			}
		}
		function evtDateStartUpdated(d) {
			if (d) {
				runInAction(() => {
					self._selectedDateStart = d;
				});
				//appStore.changeWorkingRange(d);
			}
		}

		const isRange = this.props.enableRange;
		const dtFormatted = this._selectedDate.format('DD.MM.YYYY');
		const dtStartFormatted = this._selectedDateStart.format('DD.MM.YYYY');
		const dtFormattedTitle = isRange
			? <span>Период: <b>{dtFormatted} - {dtStartFormatted}</b></span>
			: <span>Дата: <b>{dtFormatted}</b></span>
			;

		return <R.DropdownButton id="001" title={dtFormattedTitle} disabled={this.props.disabled}>
			<R.Panel>
				<div style={{ width: isRange ? "28em" : "13em" }} />
				<center>
					<If condition={isRange}>
						<DatePicker dateFormat="DD.MM.YYYY" style={{ width: "300px" }}
							inline
							selected={this._selectedDateStart}
							onChange={evtDateStartUpdated}
							locale="ru-ru"
							todayButton="Сегодня"
							isClearable={false}
							showWeekNumbers
							maxDate={this._selectedDate}
							minDate={moment().add(-3, "months")}
							placeholderText="Выберите начальную дату" />
					</If>
					<DatePicker dateFormat="DD.MM.YYYY" style={{ width: "300px" }}
						inline
						selected={this._selectedDate}
						onChange={evtDateUpdated}
						locale="ru-ru"
						todayButton="Сегодня"
						isClearable={false}
						showWeekNumbers
						maxDate={moment().add(12, "months")}
						minDate={moment().add(-3, "months")}
						placeholderText={isRange ? "Выберите конечную дату" : "Выберите рабочую дату"} />
				</center>
			</R.Panel>
		</R.DropdownButton>
	}
}
