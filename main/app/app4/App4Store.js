//@flow
import { observable, computed } from 'mobx';
import { CurrencyInfo, CurrencyData } from './../../api/types';
import { fetchCurrenciesList, fetchCurrencyByCodeAndDate } from './../../api/fetch';
import { reaction } from 'mobx';

export class App4Store {

	@observable currList: ?CurrencyInfo[];
	@observable workingDate = moment();
	@observable selectedCurrCodeRequested: ?string = null;
	@observable currData: ?CurrencyData = null;
	@observable selectedCurrCode: ?string = null;
	@observable dataLoading: boolean = false;

	constructor() {
		let self = this;
		fetchCurrenciesList().then((data) => {
			self.currList = data
		});

		reaction(
			() => [self.workingDate],
			() => {
				console.debug("Изменена рабочая дата: " + moment(this.workingDate).format("YYYY-MM-DD"))
			}
		);

		reaction(
			() => [self.selectedCurrCode],
			() => {
				console.debug("Изменена выбранная валюта: " + this.selectedCurrCode)
			}
		);

		reaction(
			() => [self.selectedCurrCode, self.workingDate],
			() => {
				if (self.selectedCurrCode != null) {
					self.dataLoading = true;
					fetchCurrencyByCodeAndDate(this.selectedCurrCode, this.workingDate)
						.then(data => {
							self.dataLoading = false;
							self.currData = data
						})
						.catch(() => {
							self.dataLoading = false;
						});
				}
				else {
					self.dataLoading = false;
					self.currData = null;
				}
			},
			true
		);

	}

	dispose() {
	}

	@computed get isDataReady(): boolean {
		return this.currList != null && !this.dataLoading;
	}

	renderSimpleList() {
		return <div>
			<ul>
				{
					this.currList.map(it => <li key={it.code}>{it.code} <br /> {it.description}</li>)
				}
			</ul>
		</div>
	}
}
