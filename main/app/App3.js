import { Component } from 'react';
import { observer } from 'mobx-react';
import { LayoutMain } from './LayoutMain';
import { observable } from 'mobx';
import { App3Store } from './app3/App3Store';
import { WorkingDateChooser } from './app3/WorkingDateChooser';
import { CurrencyChooser } from './app3/CurrencyChooser';
import * as R from 'react-bootstrap';

@observer
export default class App3 extends Component {

	@observable store: App3Store;

	componentWillMount() {
		this.store = new App3Store();
	}

	componentWillUnmount() {
		this.store.dispose();
		this.store = null;
	}

	render() {
		let workArea = <If condition={this.store.isDataReady}>
			<R.Grid>
				<R.Row>
					<R.Col xs={2}>
						<WorkingDateChooser store={this.store} />
					</R.Col>

					<R.Col xs={2}>
						<CurrencyChooser store={this.store} />
					</R.Col>
				</R.Row>
			</R.Grid>
		</If>

		return <LayoutMain title={"Валюты v3"} workArea={workArea} isLoading={!this.store.isDataReady} />
	}
}
