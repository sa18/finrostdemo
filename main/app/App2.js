import { Component } from 'react';
import { fetchCurrenciesList } from './../api/fetch';
import { CurrencyInfo } from './../api/types';
import { observer } from 'mobx-react';
import { observable, computed } from 'mobx';
import { LayoutMain } from './LayoutMain';


class AppStore {

	@observable currList: ?CurrencyInfo[];

	constructor() {
		let self = this;
		fetchCurrenciesList().then((data) => {
			self.currList = data
		});
	}

	dispose() {
	}

	@computed get isDataReady(): boolean {
		return this.currList != null;
	}

	renderSimpleList() {
		return <ul>
			{
				this.currList.map(it => <li key={it.code}>{it.code} <br /> {it.description}</li>)
			}
		</ul>
	}
}


@observer
export default class App2 extends Component {

	@observable store: AppStore;

	componentWillMount() {
		this.store = new AppStore();
	}

	componentWillUnmount() {
		this.store.dispose();
		this.store = null;
	}

	render() {
		let workArea = <If condition={this.store.isDataReady}>
			{this.store.renderSimpleList()}
		</If>

		return <LayoutMain title={"Простой список 2"} workArea={workArea} isLoading={!this.store.isDataReady} />
	}
}
