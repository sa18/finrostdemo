import { Component } from 'react';
import { LayoutMain } from './LayoutMain';

export default class App0 extends Component {

	componentWillMount() {
	}

	render() {
		const title = <div>Привет, RND.JS!</div>;

		return <LayoutMain title={title} isLoading={true} />
	}
}
