import { Component } from 'react';
import { observer, Provider } from 'mobx-react';
import { LayoutMain } from './LayoutMain';
import { observable } from 'mobx';
import { App4Store } from './app4/App4Store';
import { WorkingDateChooser } from './app4/WorkingDateChooser';
import { CurrencyChooser } from './app4/CurrencyChooser';
import * as R from 'react-bootstrap';
import { Details } from './app4/Details';

@observer
export default class App4 extends Component {

	@observable store: App4Store;

	componentWillMount() {
		this.store = new App4Store();
	}

	componentWillUnmount() {
		this.store.dispose();
		this.store = null;
	}

	render() {
		let workArea = <Provider store={this.store}>
			<R.Grid>
				<R.Row>
					<R.Col xs={2}>
						<WorkingDateChooser />
					</R.Col>

					<R.Col xs={2}>
						<CurrencyChooser />
					</R.Col>

					<R.Col xs={2}>
						<Details />
					</R.Col>
				</R.Row>
			</R.Grid>
		</Provider>;

		return <LayoutMain title={"Валюты v4"} workArea={workArea} isLoading={!this.store.isDataReady} />
	}
}
