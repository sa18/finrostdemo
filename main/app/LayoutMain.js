// @flow
import React from 'react';
import * as R from 'react-bootstrap';
import { If } from 'jsx-control-statements';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';

interface Props {
	title?: string | React.Element<any>;
	shortTitle?: string;
	workArea?: React.Element<any>,
	toolbar?: React.Element<any>;
	footer?: React.Element<any>;
	isLoading?: boolean;
}

@observer
export class LayoutMain extends React.Component<Props> {

	componentWillMount() {
	}
	componentWillReceiveProps(newProps: Props) {
	}
	render() {
		return <R.Grid>
			<R.Row id="PageHeader">
				<R.Table width="100%">
					<tbody>
						<tr>
							<td>
								<h1>{this.props.title}</h1>
							</td>
							<td>
								{this.props.toolbar}
							</td>
						</tr>
						<If condition={this.props.isLoading}>
							<tr>
								<td colSpan={2} className="loader">
									загрузка...
								</td>
							</tr>
						</If>
					</tbody>
				</R.Table>
			</R.Row>
			<R.Row id="WorkArea">
				{this.props.workArea}
			</R.Row>
			{this.props.footer && <R.Row id="Footer">
				{this.props.footer}
			</R.Row>}
		</R.Grid>
	}
}
