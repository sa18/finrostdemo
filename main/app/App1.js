import { Component } from 'react';
import { fetchCurrenciesList } from './../api/fetch';
import { CurrencyInfo } from './../api/types';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import { LayoutMain } from './LayoutMain';

export default class App1 extends Component {

	list: ?CurrencyInfo[] = null;

	componentWillMount() {
		let self = this;
		fetchCurrenciesList().then((data) => {
			self.list = data
		});
	}

	render() {
		let workArea = <If condition={this.list != null}>
			<ul>
				{
					this.list.map(item => <li>{item.code} <br /> {item.description}</li>)
				}
			</ul>
		</If>

		return <LayoutMain title={"Простой список"} workArea={workArea} isLoading={this.list == null} />
	}
}
