//@flow

import { CurrencyInfo } from './types.js';

function delayer(data) {
	return function (resolve, reject) {
		setTimeout(function () { resolve(data) }, 2000);
	}
}

export function fetchCurrenciesList(): Promise<CurrencyInfo[]> {
	const testData: CurrencyInfo[] = [
		{ code: "EURRUB", description: "Пара евро/рубль", expired: false },
		{ code: "USDRUB", description: "Пара доллар США/рубль", expired: false },
		{ code: "BTCUSD", description: "Пара Биткойн/доллар США", expired: false },
	];

	return new Promise(delayer(testData));
}

export function fetchCurrencyByCodeAndDate(code: string, date: string | number | moment): Promise<CurrencyData> {
	let value = Math.random();
	if (code == "EURRUB")
		value = 70 + 5 * value;
	else if (code == "USDRUB")
		value = 60 + 5 * value;
	else if (code == "BTCUSD")
		value = 10000 * 5000 * value;

	let data: CurrencyData = {};
	data.code = code;
	data.date = moment(date).format("YYYY-MM-DD");
	data.value = value;
	return new Promise(delayer(data));
}
