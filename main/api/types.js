// @flow

export interface CurrencyInfo {
	code: string;
	description: string;
	expired: boolean;
}

export interface CurrencyData {
	code: string;
	date: string | moment | number;
	value: number;
}
