## FinRost Demo project

>[как настроить среду разработки ES7 + webpack](https://www.valentinog.com/blog/react-webpack-babel/)

>[жизенный цикл компонентов React](http://busypeoples.github.io/post/react-component-lifecycle/)

>[как подключить LESS-стили к проекту](https://github.com/webpack-contrib/less-loader)

>[касательно Layout и Scrolling](http://blog.stevensanderson.com/2011/10/05/full-height-app-layouts-a-css-trick-to-make-it-easier/)

>[как использовать MOBX](https://www.robinwieruch.de/mobx-react/)
>[паттерны MOBX (старая статья)](https://blog.pixelingene.com/2016/10/effective-mobx-patterns-part-1/)

>[ES6 Design Patterns](http://loredanacirstea.github.io/es6-design-patterns/)
>[Learning JavaScript Design Patterns](https://addyosmani.com/resources/essentialjsdesignpatterns/book/)
>[о современном JavaScript](https://learn.javascript.ru/es-modern)
